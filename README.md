# UKT - Exercice Code Test

# Test unitaire
L'objecti est d'ecrire deux codes en python et javascript afin de faire un test unitaire.

## Python 

Nous créons un fichier ```utils.py``` contenant le code et un autre ```test_utils.py``` contenant le test sur le code.
La commande qui effectue le test est ```python3 -m unittest test_utils.py```
-    ***Ressources :***  Tout est tres bien explique sur cette documentation https://geekflare.com/fr/unit-testing-with-python-unittest/


-    Generation de rapport: https://pypi.org/project/unittest-xml-reporting/
    -    Nous avons générer un rapport dans un ficher XML

-   Rajouter des rapports de couverture de tests
    -    



## Javascript 

Nous allons utiliser une fonction qui calcule l'age.


npm run build vs npm start

**npm run build** est un alias pour npm build et ne fait rien à moins que vous ne spécifiiez ce que "build" fait dans votre fichier package. json. Il vous permet d'effectuer toutes les tâches de construction/préparation nécessaires à votre projet, avant qu'il ne soit utilisé dans un autre projet.

**npm start** crée un serveur local que vous pouvez cloudflared pour un usage personnel